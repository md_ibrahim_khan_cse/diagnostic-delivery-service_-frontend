import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Investigation } from '../Model/Invesigation';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InvestigationService {

  private saveInvestigationUrl = "http://localhost:8080/investigation/";
  private updateInvestigationUrl = "http://localhost:8080/investigation/";
  private deleteInvestigationUrl = "http://localhost:8080/investigation/";
  private investigationUrl: string = "http://localhost:8080/investigation";
  private investigationCagoryUrl: string = "http://localhost:8080/investigation";
  private investigationFetchUrl: string = 'http://localhost:8080/investigationprovider/';
  private investigationNameFetchUrl: string = 'http://localhost:8080/investigationname/';
  private fetchformat: string = "http://localhost:8080/investigation/";


  constructor(private _http: HttpClient) { }

  saveInvestigation(providerID, categoryId, investigation): Observable<any> {
    return this._http.post(this.saveInvestigationUrl + providerID + "/" + categoryId, investigation, {
      responseType: 'text' as 'json'
    });
  }

  updateInvestigation(providerID, categoryId, investigationId, investigation): Observable<any> {
    return this._http.post(this.updateInvestigationUrl + providerID + "/" + investigationId + "/" + categoryId, investigation, {
      responseType: 'text' as 'json'
    });
  }

  deleteInvestigation(providerID, investigationId): Observable<any> {
    return this._http.delete(this.deleteInvestigationUrl + providerID + "/" + investigationId, {
      responseType: 'text' as 'json'
    });
  }

  fetchInvestigationByProviderId(providerId): Observable<Investigation[]> {
    return this._http.get<Investigation[]>(this.investigationFetchUrl + providerId);
  }

  fetchInvestigation(): Observable<Investigation[]> {
    return this._http.get<Investigation[]>(this.investigationUrl);
  }

  fetchInvestigationByInvestigationIdList(selectedItems): Observable<Investigation[]> {
    return this._http.get<Investigation[]>(this.investigationUrl + "/" + selectedItems);
  }
  
  fetchInvestigationByCategoryId(catagoryId): Observable<Investigation[]> {
    return this._http.get<Investigation[]>(this.investigationCagoryUrl + "/" + catagoryId);
  }



  fetchInvestigationNameByProviderId(providerId): Observable<string[]>{
    return this._http.get<string[]>(this.investigationNameFetchUrl + providerId);
  }

  fetchFormat(providerId, investigationName) {
    return this._http.get<any>(this.fetchformat + providerId + "/" + investigationName);
  }

}
