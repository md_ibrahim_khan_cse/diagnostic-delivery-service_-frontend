import { Injectable } from '@angular/core';
import { Category } from '../Model/Category';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private categoryUrl: string = "http://localhost:8080/category";
  private searchBook_url: string = "http://localhost:8080/searchbook";



  constructor(private _http: HttpClient) { }

  fetchCategory(): Observable<Category[]> {
    return this._http.get<Category[]>(this.categoryUrl);
  }

  // searchbook(formdata: FormData):Observable<book_image[]>{
  //   return this._http.post<book_image[]>(this.searchBook_url, formdata);
  // }
}
