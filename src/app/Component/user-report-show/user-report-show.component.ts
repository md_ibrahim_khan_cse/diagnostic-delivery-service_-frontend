import { Component, OnInit } from '@angular/core';
import { InvestigationRequestServiceService } from 'src/app/Service/investigation-request-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import * as $ from 'jquery';

@Component({
  selector: 'app-user-report-show',
  templateUrl: './user-report-show.component.html',
  styleUrls: ['./user-report-show.component.css']
})
export class UserReportShowComponent implements OnInit {

  phoneNumber: string = "";
  hasParameter: boolean;
  providerId: number;
  showTable: boolean = false;
  public userReportList: any = [];

  constructor(private investigationRequestService: InvestigationRequestServiceService,
    private _avtivatedRoute: ActivatedRoute,
    private localStoregeService: LocalStorageService,
    private router: Router) { }

  ngOnInit(): void {


    document.getElementById("dismiss-popup-btn").addEventListener("click", function () {
      document.getElementsByClassName("popup")[0].classList.remove("active");
    });


    $(document).ready(function () {

      $("#gmail-popup-close").click(function () {
        $("#gmail-popup").css({ 'height': 0 });
        $("#gmail-popup").toggleClass("border-class");
      });

    });

    this.hasParameter = this._avtivatedRoute.snapshot.paramMap.has('providerId');

    if (this.hasParameter) {
      this.providerId = parseInt(this._avtivatedRoute.snapshot.paramMap.get("providerId"));
      this.localStoregeService.store("hiddenHeader", true);
    }


  }

  downloadPdf(investigationRequestId, index) {


    if (this.userReportList[index].billStatus) { 

      this.investigationRequestService.fetchPdf(this.providerId, investigationRequestId).subscribe(x => {

        var file = new Blob([x], { type: 'application/pdf' })
        var fileURL = URL.createObjectURL(file);
        console.log("id " + investigationRequestId);

        var a = document.createElement('a');
        a.href = fileURL;

        a.dispatchEvent(new MouseEvent("click", { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          window.URL.revokeObjectURL(fileURL);
          a.remove();
        }, 100);

      },
        (error) => {
          console.log('getPDF error: ', error);
        }
      );
    }
    else{
      document.getElementsByClassName("popup")[0].classList.add("active");
    }
  }

  downloadReportImage() {

    this.investigationRequestService.fetchimagereportDownload().subscribe(x => {

      var file = new Blob([x], { type: 'image/jpg' });
      console.log("file " + file);
      var fileURL = URL.createObjectURL(file);
      console.log("file " + fileURL);

      var a = document.createElement('a');
      a.href = fileURL;
      // a.download = 'testreportdd.pdf';


      // a.dispatchEvent(new MouseEvent("click", { bubbles: true, cancelable: true, view: window }));

      // setTimeout(function () {
      //   window.URL.revokeObjectURL(fileURL);
      //   a.remove();
      // }, 100);

    },
      (error) => {
        console.log('getPDF error: ', error);
      }
    );

  }


  sendMobileNumber() {


    if (this.phoneNumber != "") {

      $("#gmail-popup").css({ 'height': 0 });
      $("#gmail-popup").toggleClass("border-class");

      this.investigationRequestService.fetchUserReport(this.providerId, this.phoneNumber).subscribe(
        data => {
          this.userReportList = data;
          if (data.length == 0){
            alert("No report is found");
          }
          this.showTable = true;
        },
        error => {
          alert("An error is occured ");
        }
      );

      
      this.phoneNumber = "";
    }
    else {
      alert("Please enter your mobile number");
      
    }


    // this.localStoregeService.store("hiddenHeader", false); 01907503730
    // this.localStoregeService.store('providerId', this.providerId);
    // this.router.navigate(['/userreportlist']);
  }

}
