import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import * as $ from 'jquery';
import { InvestigationRequestServiceService } from 'src/app/Service/investigation-request-service.service';
import { InvestigationService } from 'src/app/Service/investigation.service';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-admin-investigation-post',
  templateUrl: './admin-investigation-post.component.html',
  styleUrls: ['./admin-investigation-post.component.css']
})
export class AdminInvestigationPostComponent implements OnInit {


  investigationPostForm: FormGroup;
  investigationRequestId: number;
  providerId: number;
  public formdata = new FormData();
  public reportimage: any = File;
  defaultTime = { hour: 13, minute: 30 };
  meridian = true;
  gamilValue: string;
  imageUrl: string | ArrayBuffer;
  investigationformat: string = "";
  curDate = new Date();
  reportImageIsSelected: boolean = false;

  ptientName: string = "Not set";
  age: string = "Not set";
  sex: string = "Not set";
  sample: string = "Not set";
  reportResult: string = "Not set";
  referenceValue: string = "Not set";
  investigationName: string = "Not set";

  investigationNameArray = [
    "ibrahim",
    "rifat",
    "hasiv"
  ]


  constructor(private formBuilder: FormBuilder, private investigationRequestService: InvestigationRequestServiceService,
    private invesgationRequest: InvestigationService,
    private localStoregeService: LocalStorageService) {


    this.investigationPostForm = formBuilder.group({
      'patientName': [null, Validators.required],
      'mobile': [null, Validators.required],
      'investigationName': [null, Validators.required],
      'reportText': [null, Validators.required],
      'referenceId': [null, Validators.required],
      'billStatus': [null, Validators.required],
      'gender': [null, Validators.required],
      'age': [null, Validators.required],
      'gmail': [null, Validators.required]
    });

  }

  ngOnInit(): void {


    this.providerId = this.localStoregeService.retrieve("providerId");

    this.invesgationRequest.fetchInvestigationNameByProviderId(this.providerId).subscribe(
      data => this.investigationNameArray = data
    );


// jquery
    document.getElementById("dismiss-popup-btn").addEventListener("click", function () {
      document.getElementsByClassName("popup")[0].classList.remove("active");
    });

    $(document).ready(function () {

      $("#successfull-next-popup-close").click(function () {
        $("#successfull-next-popup").css({ 'height': 0 });
        $("#successfull-next-popup").toggleClass("border-class");
      });


      $("#gmail-popup-close").click(function () {
        $("#gmail-popup").css({ 'height': 0 });
        $("#gmail-popup").toggleClass("border-class");
      });

    });


  }

  investigationPostFormSubmit(form: NgForm) {

    // this.investigationPostForm.value.name = "fff";
    // this.investigationPostForm.value.mobile = "kk"; ebrahimkhanobak@gmail.com

    if (this.investigationPostForm.value.patientName != null &&
      this.investigationPostForm.value.mobile != null &&
      this.investigationPostForm.value.investigationName != null) {

      this.formdata.append('object', JSON.stringify(this.investigationPostForm.value));
      this.formdata.append('reportImage', this.reportimage);


      this.gamilValue = this.investigationPostForm.value.gmail;
      this.ptientName = this.defaultSet(this.investigationPostForm.value.patientName);
      this.sex = this.defaultSet(this.investigationPostForm.value.gender);
      this.age = this.defaultSet(this.investigationPostForm.value.age);
      this.reportResult = this.defaultSet(this.investigationPostForm.value.reportText);
      this.investigationName = this.defaultSet(this.investigationPostForm.value.investigationName);

      // console.log("patient age " + this.age + " " + this.investigationPostForm.value.age);

      // console.log("form data=" + this.formdata.getAll("object"));
      // console.log("form data=" + this.formdata.getAll("reportImage"));



      this.investigationRequestService.adminInvestigationRequest(this.providerId, this.formdata).subscribe(
        data => {

          $("#successfull-next-popup").css({ 'height': 500 });
          $("#successfull-next-popup").toggleClass("border-class");
          this.investigationRequestId = data;
        },
        error => {
          console.log(error.data)
          alert("An error is occured");
        }
      );


      this.formdata.delete("object");
      this.formdata.delete("reportImage");

    }
    else {
      let key = Object.keys(this.investigationPostForm.controls);

      key.filter(data => {
        let control = this.investigationPostForm.controls[data];

        if (control.errors != null) {
          control.markAllAsTouched();
        }
      });
    }
  }

  onselectfile(event) {
    const file = event.target.files[0];
    this.reportimage = file;

    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = event => {
      this.imageUrl = reader.result;
    };

    if (event.target.files.length != 0){
      this.reportImageIsSelected = true;
    }

  }


  format() {
    console.log("value " + this.investigationPostForm.value.investigationName);

    this.invesgationRequest.fetchFormat(this.providerId, this.investigationPostForm.value.investigationName).subscribe(
      data => {
        this.investigationformat = data.format;
        this.sample = data.sample;
        this.referenceValue = data.referenceValue;
        console.log("data " + data);
      }
    );
  }

  nextPdfSend() {


    $("#gmail-popup").css({ 'height': 180 });
    $("#gmail-popup").toggleClass("border-class");

    $("#successfull-next-popup").css({ 'height': 0 });
    $("#successfull-next-popup").toggleClass("border-class");

  }

  pdfSend() {

    if (this.gamilValue != null && this.gamilValue != "" && this.investigationRequestId != null &&
      this.providerId != null) {

      this.investigationRequestService.sendEmailToPatient(this.investigationRequestId, this.providerId, this.gamilValue).subscribe(
        data => {
          alert("Email is successfully send");
          console.log("data  " + data);
          $("#successfull-next-popup").css({ 'height': 0 });
          $("#successfull-next-popup").toggleClass("border-class");
        },
        error => {
          console.log(error.data)
          alert("An error is occured");
          // $("#gmail-popup").css({ 'height': 0 });
          // $("#gmail-popup").toggleClass("border-class");
        }
      );


    }
    else {
      alert("Please enter gmail");
    }
  }

  close() {


    $("#successfull-next-popup").css({ 'height': 0 });
    $("#successfull-next-popup").toggleClass("border-class");
  }

  defaultSet(value) {

    if (value == null || value == "") {
      return "Not set";
    }
    return value;
    // this.age = "Not set";
    // this.sex = "Not set";
    // this.ptientName = "Not set";
    // this.reportResult = "Not set"
    // this.sample = "Not set";
    // this.referenceValue = "Not set";
  }

}
