import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { Category } from 'src/app/Model/Category';
import { Investigation } from 'src/app/Model/Invesigation';
import { InteractionService } from 'src/app/Service/interaction.service';
import { InvestigationRequestServiceService } from 'src/app/Service/investigation-request-service.service';
import { InvestigationService } from 'src/app/Service/investigation.service';
import { CategoryService } from 'src/app/Service/category.service';
import * as $ from 'jquery';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-add-investigation',
  templateUrl: './add-investigation.component.html',
  styleUrls: ['./add-investigation.component.css']
})
export class AddInvestigationComponent implements OnInit {


  investigationForm: FormGroup;
  investigation: Investigation = {
    "refValue": "",
    "rate": 0,
    "note": "",
    "name": "",
    "id": 0,
    "format": "",
    "description": "",
    "category": null
  };
  providerId: string;
  categoryId: string;
  investigationId: string;
  categoryArray: Category[] = [
    {
      "name": "name",
      "lastModifiedDate": "name",
      "description": "name",
      "createdDate": "name",
      "id": 1
    }
  ]


  constructor(private formBuilder: FormBuilder,
    private localStoregeService: LocalStorageService, private router: Router,
    private investigationRequestService: InvestigationRequestServiceService,
    private investigationService: InvestigationService,
    private interactionService: InteractionService,
    private categoryService: CategoryService
  ) {

    this.investigationForm = formBuilder.group({
      'name': [null, Validators.required],
      'rate': [null, Validators.required],
      'format': [null, Validators.required],
      'refValue': [null, Validators.required],
      'category': [null, Validators.required]
    });

  }

  ngOnInit(): void {

    $(document).ready(function () {

    });


    // document.getElementById("open-popup-btn").addEventListener("click", function () {
    //   document.getElementsByClassName("popup")[0].classList.add("active");
    // });

    document.getElementById("dismiss-popup-btn").addEventListener("click", function () {
      document.getElementsByClassName("popup")[0].classList.remove("active");
    });

    this.providerId = this.localStoregeService.retrieve("providerId");
    if (this.providerId != null) {

      this.categoryService.fetchCategory().subscribe(

        data => {
          this.categoryArray = data;
        },
        error => {
          alert("An error is occured");
        }
      );

    }
    else {
      this.router.navigate(['']);
    }

  }


  investigationFormSubmit(form: NgForm) {


    if (this.investigationForm.valid) {

      this.investigation.name = this.investigationForm.value.name;
      this.investigation.rate = this.investigationForm.value.rate;
      this.investigation.refValue = this.investigationForm.value.refValue;
      this.investigation.format = this.investigationForm.value.format;
      this.categoryId = this.investigationForm.value.category;

      //console.log("value " + JSON.stringify(this.investigationForm.value));


      this.investigationService.saveInvestigation(this.providerId, this.categoryId, this.investigation).subscribe(
        data => {
          document.getElementsByClassName("popup")[0].classList.add("active");
        },
        error => {
          alert("Fail to updated");
        }
      );
    }
    else {

      let key = Object.keys(this.investigationForm.controls);

      key.filter(data => {
        let control = this.investigationForm.controls[data];

        if (control.errors != null) {
          control.markAllAsTouched();
        }
      });
      //alert("Fill the form properly");
    }

  }

}
