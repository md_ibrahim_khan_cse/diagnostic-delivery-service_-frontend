import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public id : any = "";
  hospitalTitle: string;
  hospitalImage: string = "../assets/medicalCollage/" + 1 + ".jpg";
  public hide : boolean = true;
  private currentUrl;

  medicalName: string[] = ['Khulna Medical Hospital', 'Shaheed Sheikh Abu Naser Specialised Hospital',
    'Khulna City Hospital', 'Islami Bank Hospital Khulna', 'Khulna Shishu Hospital', 'Gazi Medical College and Hospital']


  constructor(private router: Router,
    private localStoregeService: LocalStorageService) { }

  ngOnInit(): void {

    $(document).ready(function () {


      // $("#investigation-request-list-popup-close").click(function () {
      //   $("#investigation-request-list-popup").css({ 'height': 0 });
      //   $("#investigation-request-list-popup").toggleClass("border-class");
      // });

      //alert("i am the show off");

    });

    // this.interactionService.headingTitle$.subscribe(
    //   id => {
    //     this.id = id
    //     this.hospitalTitle = this.medicalName[this.id - 1];
    //     this.hide = true;
    //   }
    // )

    if (window.location.href == 'http://localhost:4200/') {
      this.hide = false;
    }
  }

  simple(){

    if (window.location.href == 'http://localhost:4200/') {
      this.hide = false;
    }
    else{
      this.hide = true;
    }
    console.log("simple " + this.hide);
  }

}
