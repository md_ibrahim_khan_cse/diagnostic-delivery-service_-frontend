import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { Category } from 'src/app/Model/Category';
import { Investigation } from 'src/app/Model/Invesigation';
import { InteractionService } from 'src/app/Service/interaction.service';
import { InvestigationRequestServiceService } from 'src/app/Service/investigation-request-service.service';
import { InvestigationService } from 'src/app/Service/investigation.service';
import * as $ from 'jquery';
import { CategoryService } from 'src/app/Service/category.service';

@Component({
  selector: 'app-investigation-list',
  templateUrl: './investigation-list.component.html',
  styleUrls: ['./investigation-list.component.css']
})
export class InvestigationListComponent implements OnInit {

  investigationForm: FormGroup;
  investigation: Investigation = {
    "refValue": "",
    "rate": 0,
    "note": "",
    "name": "",
    "id": 0,
    "format": "",
    "description": "",
    "category": null
  };
  providerId: string;
  categoryId: string;
  investigationId: string;
  investigationList : Investigation[];
  categoryArray: Category[] = [
    {
      "name": "name",
      "lastModifiedDate": "name",
      "description": "name",
      "createdDate": "name",
      "id": 1
    }
  ];
  public popupMessage : string = ""


  constructor(private formBuilder: FormBuilder,
    private localStoregeService: LocalStorageService, private router: Router,
    private investigationRequestService: InvestigationRequestServiceService,
    private investigationService: InvestigationService,
    private interactionService: InteractionService,
    private categoryService: CategoryService
  ) {

    this.investigationForm = formBuilder.group({
      'name': [null, Validators.required],
      'rate': [null, Validators.required],
      'format': [null, Validators.required],
      'refValue': [null, Validators.required],
      'category': [null, Validators.required]
    });

  }

  ngOnInit(): void {

    document.getElementById("dismiss-popup-btn").addEventListener("click", function () {
      document.getElementsByClassName("popup")[0].classList.remove("active");
    });

    $(document).ready(function () {

      $("#update-form-popup-close").click(function () {
        $("#update-form-popup").css({ 'opacity': 0 });
        $("#update-form-popup").toggleClass("border-class");
        setTimeout(function () { $("#update-form-popup").css({ 'height': 0 });; }, 200);
        // $("#update-form-popup").css({ 'height': 0 });
        
      });

    });


    this.providerId = this.localStoregeService.retrieve("providerId");
    if (this.providerId != null){
      this.fetchInvestigationData();

      this.categoryService.fetchCategory().subscribe(

        data => {
          this.categoryArray = data;
        },
        error => {
          alert("n error is occured");
        }
      );

    }
    else{
      this.router.navigate(['']);
    }

  }


  investigationFormSubmit(form: NgForm) {


    if (this.investigationForm.valid) {

      this.investigation.name = this.investigationForm.value.name;
      this.investigation.rate = this.investigationForm.value.rate;
      this.investigation.refValue = this.investigationForm.value.refValue;
      this.investigation.format = this.investigationForm.value.format;
      this.categoryId = this.investigationForm.value.category;


      this.investigationService.updateInvestigation(this.providerId, this.categoryId, this.investigationId, this.investigation).subscribe(
        data => {
          
          
          this.openPopUp("Successfully Updated");
          $("#update-form-popup").css({ 'height': 0 });
          $("#update-form-popup").removeClass("border-class");
          this.fetchInvestigationData();
        },
        error => {
          this.openPopUp("Fail to update");
        }
      );
    }
    else {
      alert("Fill the properly");
    }

  }

  updateOpenForm(investigationId, index){
    //this.investigationForm.value.name = "Md Ibrahim Khan";
    this.investigationForm.get('name').setValue(this.investigationList[index].name);
    this.investigationForm.get('rate').setValue(this.investigationList[index].rate);
    this.investigationForm.get('format').setValue(this.investigationList[index].format);
    this.investigationForm.get('refValue').setValue(this.investigationList[index].refValue);
    this.investigationForm.get('category').setValue(this.investigationList[index].category.id);



    $("#update-form-popup").css({ 'opacity': 1 });
    $("#update-form-popup").css({ 'height': 400 });
    $("#update-form-popup").addClass("border-class");
    this.investigationId = investigationId;
  }

  deleteOpen(investigationId){
    this.investigationId = investigationId;
    $("#delete-warning-popup").css({ 'top': 250 });
    $("#delete-warning-popup").css({ 'opacity': 1 });
    //$("#delete-warning-popup").toggleClass("border-class");
    // setTimeout(function () { $("#delete-warning-popup").css({ 'opacity': 1 }); }, 100);
  }


  delete() {

    this.investigationService.deleteInvestigation(this.providerId, this.investigationId).subscribe(
      data => {
        this.openPopUp("Successfully Deleted");
        this.fetchInvestigationData();
      },
      error => {
        this.openPopUp("Fail to delete");
      }
    );

    $("#delete-warning-popup").css({ 'opacity': 0 });
    setTimeout(function () { $("#delete-warning-popup").css({ 'top': -250 });; }, 100);
  }


  warningCancle(){

    $("#delete-warning-popup").css({ 'opacity': 0 });
    //$("#delete-warning-popup").toggleClass("border-class");
    setTimeout(function () { $("#delete-warning-popup").css({ 'top': -250 });; }, 500);

  }

  fetchInvestigationData(){
    this.investigationService.fetchInvestigationByProviderId(this.providerId).subscribe(
      data => {
        this.investigationList = data;
      },
      error => alert("An error is occured")
    );
  }

  openPopUp(message) {
    this.popupMessage = message;
    document.getElementsByClassName("popup")[0].classList.add("active");
  }
}
