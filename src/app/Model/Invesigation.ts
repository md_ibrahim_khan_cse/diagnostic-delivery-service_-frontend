import { Category } from './Category';

export interface Investigation {
    'id' : Number,
    'name': string,
    'description': string,
    'note': string,
    'rate': number,
    'refValue' : string,
    'format' : string,
    'category' : Category
}